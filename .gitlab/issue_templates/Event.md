### Event 

Event Details: 
* http:
* Location: 
* Speakers: 
* Attendees:
* Staffing Needs:
* [Sponsorship]():
* Swag: 
* Lodging: 
* Expo Center: 
* Audience Demographics/ Breakdown
* Expected returns (in terms of MQLs):
* Other Benefits of attending/ sponsorship:
* Total costs (sponsorship + travel + collateral + etc.):


Event Checklist:
* [ ] Contract Signed
* [ ] Artwork sent
* [ ] Booth Art Work sent- custom artwork included in package 
* [ ] Speaker Brief Sent
* [ ] Tickets allocated   
* [ ] Attendee directory
* [ ] Press list sent 
* [ ] BDRs set up in person meetings- will get company list 2 weeks before 
* [ ] Badge scanners
* [ ] Booth duty scheduling
* [ ] Social media copy written and scheduled
* [ ] Select hotel
* [ ] Everyone to book into same hotel
* [ ] flights/ transport booked
* [ ] Meetings and Sales activity pre Event
* [ ] Everyone to add profile to event webiste where possible
* [ ] Download Event App
* [ ] Scrape list of attendees
* [ ] If not available pass list of attendees and speakers to Leadware
* [ ] Create Meeting Schedule in calendly?
* [ ] Mailing from sfdc / marketo
* [ ] Join LinkedIn group
* [ ] After event follow up set up in Marketo
* [ ] After event survey sent 

## Expo Schedule:


## Other:

@emilykyle